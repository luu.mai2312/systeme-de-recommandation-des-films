# Systeme de Recommandation des films

- Equipe: Mai LUU, Esther GOMBIN, Cédric DUBREUIL, Philippe BENISSAN
<br>

- Objectif :

Créer un système de recommandation de produits basé sur des datasets open sources.
<br>

- Environnement :
  - Jupyter NoteBook
  - Python
  - Pandas
  - Numpy
  - Matplotlib
  - Seaborn
  - Scikit-learn
  - Tkinter
<br>

- Sources des données exploitées :
    - IMDB: https://datasets.imdbws.com
    - MovieLens: https://grouplens.org/datasets/movielens/latest/ 
<br>

- Techniques :
    - EDA : Exploratory Data Analysis
    - Data visualisation
<br>

- Screenshot de l'interface
![Exemple](Interface_Tkinter.png)
